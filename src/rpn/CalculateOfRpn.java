package rpn;

import java.util.List;
import java.util.Stack;
import java.util.regex.Pattern;

import exceptions.CalculationException;

public class CalculateOfRpn {

	InifixToRpn inifixToRpn = new InifixToRpn();
	private final Pattern OPERATIONS_PATTERN = Pattern.compile("\\*|\\/|\\+|-|\\^");

	public String calculateValueOfRpn(List<String> listOfString) throws NumberFormatException, CalculationException {
		Stack<String> stack = new Stack<>();

		for (String x : listOfString) {
			if (!OPERATIONS_PATTERN.matcher(x).matches()) {
				stack.add(x);
			} else {
				for (Marks operator : Marks.values()) {
					if (operator.getPattern().matcher(x).matches()) {
						Double secondFactor = Double.parseDouble(stack.pop());
						Double firstFactor = Double.parseDouble(stack.pop());
						stack.add(String.valueOf(operator.getOperations().makeOperation(firstFactor, secondFactor)));
						break;
					}
				}
			}
		}
		
		return stack.pop();
	}
}
