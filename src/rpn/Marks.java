package rpn;

import java.util.regex.Pattern;

import core.Divide;
import core.Minus;
import core.Operations;
import core.Plus;
import core.Power;
import core.Multiply;

public enum Marks {
	PLUS {
		@Override
		public int getPriority() {
			return 1;
		}

		@Override
		public Pattern getPattern() {
			return Pattern.compile("[\\+]");
		}

		@Override
		public Operations getOperations() {
			
			return new Plus();
		}
		
	},
	MINUS {
		@Override
		public int getPriority() {
			return 1;
		}

		@Override
		public Pattern getPattern() {
			return Pattern.compile("[\\-]");
		}

		@Override
		public Operations getOperations() {
			
			return new Minus();
		}
	},
	DIVIDE {
		@Override
		public int getPriority() {
			return 2;
		}

		@Override
		public Pattern getPattern() {
			return Pattern.compile("[\\/]");
		}

		@Override
		public Operations getOperations() {
		
			return new Divide();
		}
	},
	MULTIPLY {
		@Override
		public int getPriority() {
			return 2;
		}

		@Override
		public Pattern getPattern() {
			return Pattern.compile("[\\*]");

		}

		@Override
		public Operations getOperations() {
			
			return new Multiply();
		}
	},
	POWER {
		@Override
		public int getPriority() {
			return 3;
		}

		@Override
		public Pattern getPattern() {
			return Pattern.compile("[\\^]");

		}

		@Override
		public Operations getOperations() {
			
			return new Power();
		}
	},
	LEFT_BRACKET {
		@Override
		public int getPriority() {
			return 0;
		}

		@Override
		public Pattern getPattern() {
			return Pattern.compile("[\\(]");

		}

		@Override
		public Operations getOperations() {
			// TODO Auto-generated method stub
			return null;
		}
	},
	RIGHT_BRACKET {
		@Override
		public int getPriority() {
			return 1;
		}

		@Override
		public Pattern getPattern() {
			return Pattern.compile("[\\)]");
		}

		@Override
		public Operations getOperations() {
			// TODO Auto-generated method stub
			return null;
		}
	},
	NUMBER {
		@Override
		public int getPriority() {
			return -1;
		}

		@Override
		public Pattern getPattern() {
			return Pattern.compile("[0-9]+(\\.[0-9]+)?");
		}

		@Override
		public Operations getOperations() {
			// TODO Auto-generated method stub
			return null;
		}
	};

	public abstract int getPriority();

	public abstract Pattern getPattern();

	public abstract Operations getOperations();
	
}
