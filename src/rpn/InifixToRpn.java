package rpn;

import java.util.ArrayList;
import java.util.List;
import java.util.Stack;
import java.util.function.Predicate;
import java.util.regex.Pattern;

import exceptions.RpnException;

public class InifixToRpn {

	private final Pattern singleNumberPattern = Pattern.compile("^([0-9]+|\\.)$");
	private final Pattern numberPattern = Pattern.compile("[0-9]+(\\.[0-9]+)?");

	public List<String> convertToRpn(String algExpression) throws RpnException {
		List<String> exit = new ArrayList<>();
		Stack<String> stack = new Stack<>();
		
		for (int i = 0; i < algExpression.length(); i++) {
			String singleChar = getNumberFromString(algExpression, i);
			int priorityOfSingleChar = searchMarks(getNumberFromString(algExpression, i));
			int counter = singleChar.length() - 1;
			if (singleChar.length() > 1) {
				i += counter;
			}

			if (priorityOfSingleChar == -1) {
				exit.add(singleChar);
			}
			if (priorityOfSingleChar == 0) {
				stack.add(singleChar);
			}
			if (priorityOfSingleChar == 1 || priorityOfSingleChar == 2 || priorityOfSingleChar == 3) {
				if (stack.isEmpty() || (priorityOfSingleChar > searchMarks(stack.peek()))) {
					stack.add(singleChar);
				} else {
					while (!stack.isEmpty() && searchMarks(stack.peek()) >= priorityOfSingleChar) {
						exit.add(stack.pop());
					}
					stack.add(singleChar);
				}
			}

			if (Marks.RIGHT_BRACKET.getPattern().matcher(singleChar).matches()) {
				while (!stack.isEmpty() && !Marks.LEFT_BRACKET.getPattern().matcher(stack.peek()).matches()) {
					exit.add(stack.pop());
				}
				stack.pop();
			}
		}
		while (!stack.isEmpty()) {
			exit.add(stack.pop());
		}
		
		exit.removeIf(x -> {
			if (Marks.RIGHT_BRACKET.getPattern().matcher(x).matches()
					|| Marks.LEFT_BRACKET.getPattern().matcher(x).matches()) {
				return true;
			} else {
				return false;
			}
		});

		return exit;

	}

	public String getNumberFromString(String algExpression, int j) {
		String value;
		if (singleNumberPattern.matcher(String.valueOf(algExpression.charAt(j))).matches()) {
			int i = j + 1;
			while (algExpression.length() > i
					&& singleNumberPattern.matcher(String.valueOf(algExpression.charAt(i))).matches()) {
				value = algExpression.substring(j, i + 1);
				i++;
			}
			value = algExpression.substring(j, i);
		} else {
			value = algExpression.substring(j, j + 1);
		}
		return value;

	}

	private int searchMarks(String verifyString) throws RpnException {
		for (Marks x : Marks.values()) {
			if (x.getPattern().matcher(verifyString).matches()) {
				return x.getPriority();
			}

		}

		throw new RpnException("Znak nie pasuje do ci�gu");
	}

	public String convertListToString(List<String> list) {
		StringBuilder builder = new StringBuilder();
		for (String s : list) {
			builder.append(s);
		}
		return builder.toString();
	}

}
