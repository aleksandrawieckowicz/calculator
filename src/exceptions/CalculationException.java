package exceptions;

public class CalculationException extends Exception {

	private static final long serialVersionUID = -73254271478975106L;

	public CalculationException() {
		super();
	}

	public CalculationException(String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

	public CalculationException(String message, Throwable cause) {
		super(message, cause);
	}

	public CalculationException(String message) {
		super(message);
	}

	public CalculationException(Throwable cause) {
		super(cause);
	}

}
