package exceptions;

public class RpnException extends Exception {

	private static final long serialVersionUID = 1L;

	public RpnException() {
		super();
	}

	public RpnException(String arg0, Throwable arg1, boolean arg2, boolean arg3) {
		super(arg0, arg1, arg2, arg3);
	}

	public RpnException(String arg0, Throwable arg1) {
		super(arg0, arg1);
	}

	public RpnException(String arg0) {
		super(arg0);
	}

	public RpnException(Throwable arg0) {
		super(arg0);
	}

}
