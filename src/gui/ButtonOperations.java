package gui;

import javax.swing.JButton;

import core.Operations;

public class ButtonOperations extends JButton {

	private static final long serialVersionUID = 1L;

	Operations operation;

	public ButtonOperations(String text, Operations operation) {
		super(text);
		this.operation = operation;
	}

	public Operations getOperation() {
		return operation;
	}

	
	

}
