package gui;

import javax.swing.JButton;

public class ButtonNumbers extends JButton {

	private static final long serialVersionUID = 7023881077647093061L;

	String number;

	public ButtonNumbers(int number) {
		super(String.valueOf(number));
		this.number = String.valueOf(number);
	}

	public String getNumber() {
		return number;

	}

}
