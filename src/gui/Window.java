package gui;

import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridBagLayout;

import javax.swing.JButton;
import javax.swing.JFrame;

public class Window extends JFrame {
	
	
	 
	public Window() {
		super("Kalkulator");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		pack();
		setVisible(true);
		setSize(700,500);
		setLocation(50,50);
		GamePanel GamePanel = new GamePanel();
		add(GamePanel);
	}

}
