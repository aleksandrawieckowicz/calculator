package gui;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.regex.Pattern;

import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JTextField;

import core.Divide;
import core.Minus;
import core.Multiply;
import core.Operations;
import core.Plus;
import exceptions.CalculationException;
import exceptions.RpnException;
import rpn.CalculateOfRpn;
import rpn.CalculateOfRpnTest;
import rpn.InifixToRpn;

public class GamePanel extends JPanel {
	private static final long serialVersionUID = 800808150158601701L;
	
	private JTextField calculations;
	private String a;
	private String b;
	private Operations currentOperation;
	private final JButton button1;
	private final JButton button2;
	private final JButton button3;
	private final JButton button4;
	private final JButton button5;
	private final JButton button6;
	private final JButton button7;
	private final JButton button8;
	private final JButton button9;
	private final JButton button0;
	private final JButton buttonDot;
	private final JButton buttonPlus;
	private final JButton buttonMinus;
	private final JButton buttonDivide;
	private final JButton buttonMultiply;
	private final JButton buttonDelete;
	private final JButton buttonEquals;
	private final JButton buttonLeftBracket;
	private final JButton buttonRightBracket;
	private final JButton buttonPower;
	
	InifixToRpn rpn = new InifixToRpn();
	
	CalculateOfRpn finallyResult = new CalculateOfRpn();

	public GamePanel() {
		this.setLayout(new GridBagLayout());

		GridBagConstraints c = new GridBagConstraints();

		c.fill = GridBagConstraints.HORIZONTAL;

		buttonDelete = new JButton("DEL");

		// c.weightx = 0.5;
		c.fill = GridBagConstraints.HORIZONTAL;
		// c.gridwidth = 4;
		c.gridx = 2;
		c.gridy = 1;
		add(buttonDelete, c);

		buttonDelete.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				if (!calculations.getText().isEmpty()) {
					calculations.setText(calculations.getText().substring(0, calculations.getText().length() - 1));
				}
			}
		});

		button7 = new ButtonNumbers(7);
		c.fill = GridBagConstraints.HORIZONTAL;
		// c.weightx = 0.5;
		c.gridx = 0;
		c.gridy = 2;
		add(button7, c);

		button7.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				ButtonNumbers number = (ButtonNumbers) e.getSource();
					
					calculations.setText(calculations.getText() + number.getNumber());
				
			}
		});
		
		button8 = new ButtonNumbers(8);
		c.fill = GridBagConstraints.HORIZONTAL;
		// c.weightx = 0.5;
		c.gridx = 1;
		c.gridy = 2;
		add(button8, c);

		button8.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				ButtonNumbers number = (ButtonNumbers) e.getSource();
					calculations.setText(calculations.getText() + number.getNumber());
			}
		});

		button9 = new ButtonNumbers(9);
		c.fill = GridBagConstraints.HORIZONTAL;
		// c.weightx = 0.5;
		c.gridx = 2;
		c.gridy = 2;
		add(button9, c);

		button9.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				ButtonNumbers number = (ButtonNumbers) e.getSource();
					calculations.setText(calculations.getText() + number.getNumber());
			}
		});

		buttonDivide = new ButtonOperations("/", new Divide());
		c.fill = GridBagConstraints.HORIZONTAL;
		// c.weightx = 0.5;
		c.gridx = 3;
		c.gridy = 2;
		add(buttonDivide, c);

		buttonDivide.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				calculations.setText(calculations.getText() + "/");
				
			}
		});

		button4 = new ButtonNumbers(4);
		c.fill = GridBagConstraints.HORIZONTAL;
		// c.weightx = 0.5;
		c.gridx = 0;
		c.gridy = 3;
		add(button4, c);

		button4.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				ButtonNumbers number = (ButtonNumbers) e.getSource();
					calculations.setText(calculations.getText() + number.getNumber());
			}
		});

		button5 = new ButtonNumbers(5);
		c.fill = GridBagConstraints.HORIZONTAL;
		// c.weightx = 0.5;
		c.gridx = 1;
		c.gridy = 3;
		add(button5, c);

		button5.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				ButtonNumbers number = (ButtonNumbers) e.getSource();
					calculations.setText(calculations.getText() + number.getNumber());
			}
		});

		button6 = new ButtonNumbers(6);
		c.fill = GridBagConstraints.HORIZONTAL;
		// c.weightx = 0.5;
		c.gridx = 2;
		c.gridy = 3;
		add(button6, c);

		button6.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				ButtonNumbers number = (ButtonNumbers) e.getSource();
					calculations.setText(calculations.getText() + number.getNumber());
			}
		});

		buttonMultiply = new ButtonOperations("*", new Multiply());
		c.fill = GridBagConstraints.HORIZONTAL;
		// c.weightx = 0.5;
		c.gridx = 3;
		c.gridy = 3;
		add(buttonMultiply, c);

		buttonMultiply.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				calculations.setText(calculations.getText() + "*");
			}
		});

		button1 = new ButtonNumbers(1);
		c.fill = GridBagConstraints.HORIZONTAL;
		// c.weightx = 0.5;
		c.gridx = 0;
		c.gridy = 4;
		add(button1, c);

		button1.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				ButtonNumbers number = (ButtonNumbers) e.getSource();
					calculations.setText(calculations.getText() + number.getNumber());
			}
		});

		button2 = new ButtonNumbers(2);
		c.fill = GridBagConstraints.HORIZONTAL;
		// c.weightx = 0.5;
		c.gridx = 1;
		c.gridy = 4;
		add(button2, c);

		button2.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				ButtonNumbers number = (ButtonNumbers) e.getSource();
					calculations.setText(calculations.getText() + number.getNumber());
			}
		});

		button3 = new ButtonNumbers(3);
		c.fill = GridBagConstraints.HORIZONTAL;
		// c.weightx = 0.5;
		c.gridx = 2;
		c.gridy = 4;
		add(button3, c);

		button3.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				ButtonNumbers number = (ButtonNumbers) e.getSource();
					calculations.setText(calculations.getText() + number.getNumber());
			}
		});

		buttonMinus = new ButtonOperations("-", new Minus());
		c.fill = GridBagConstraints.HORIZONTAL;
		// c.weightx = 0.5;
		c.gridx = 3;
		c.gridy = 4;
		add(buttonMinus, c);

		buttonMinus.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				calculations.setText(calculations.getText() + "-");
			}
		});

		button0 = new ButtonNumbers(0);
		c.fill = GridBagConstraints.HORIZONTAL;
		// c.weightx = 0.5;
		c.gridx = 0;
		c.gridy = 5;
		add(button0, c);

		button0.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				ButtonNumbers number = (ButtonNumbers) e.getSource();
					calculations.setText(calculations.getText() + number.getNumber());
			}
		});

		buttonDot = new JButton(".");
		c.fill = GridBagConstraints.HORIZONTAL;
		// c.weightx = 0.5;
		c.gridx = 1;
		c.gridy = 5;
		add(buttonDot, c);

		buttonDot.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub

			}
		});

		buttonEquals = new JButton("=");
		c.fill = GridBagConstraints.HORIZONTAL;
		// c.weightx = 0.5;
		c.gridx = 2;
		c.gridy = 5;
		add(buttonEquals, c);

		buttonEquals.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				
				try {
					calculations.setText(finallyResult.calculateValueOfRpn(rpn.convertToRpn(calculations.getText())));
				} catch (RpnException e1) {
					e1.printStackTrace();
				} catch (NumberFormatException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				} catch (CalculationException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
		});

		buttonPlus = new ButtonOperations("+", new Plus());
		c.fill = GridBagConstraints.HORIZONTAL;
		// c.weightx = 0.5;
		c.gridx = 3;
		c.gridy = 5;
		add(buttonPlus, c);

		buttonPlus.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				calculations.setText(calculations.getText() + "+");
			}
		});
		
		buttonLeftBracket = new JButton("(");
		c.fill = GridBagConstraints.HORIZONTAL;
	   //c.weightx = 0.5;
		c.gridx = 0;
		c.gridy = 1;
		add(buttonLeftBracket, c);

		buttonLeftBracket.addActionListener(new ActionListener() {
		@Override
		public void actionPerformed(ActionEvent e) {
				calculations.setText(calculations.getText() + "(");
			}
	});

		buttonRightBracket = new JButton(")");
		c.fill = GridBagConstraints.HORIZONTAL;
	   //c.weightx = 0.5;
		c.gridx = 1;
		c.gridy = 1;
		add(buttonRightBracket, c);

		buttonRightBracket.addActionListener(new ActionListener() {
		@Override
		public void actionPerformed(ActionEvent e) {
				calculations.setText(calculations.getText() + ")");
			}
	});
		
		buttonPower= new JButton("^");
		c.fill = GridBagConstraints.HORIZONTAL;
	   //c.weightx = 0.5;
		c.gridx = 3;
		c.gridy = 1;
		add(buttonPower, c);

		buttonPower.addActionListener(new ActionListener() {
		@Override
		public void actionPerformed(ActionEvent e) {
				calculations.setText(calculations.getText() + "^");
			}
	});
		
		calculations = new JTextField("");
		c.gridx = 0;
		c.gridy = 0;
		c.gridwidth = 4;
		add(calculations, c);

		

	}
} 
