package core;

import exceptions.CalculationException;

public class Minus implements Operations{

	@Override
	public double makeOperation(double a, double b) throws CalculationException  {
		return a-b;
	}

}
