package core;

import exceptions.CalculationException;

public interface Operations {
	
	double makeOperation(double a, double b) throws CalculationException;
}
