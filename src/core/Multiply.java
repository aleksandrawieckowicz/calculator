package core;

import exceptions.CalculationException;

public class Multiply implements Operations {

	@Override
	public double makeOperation(double a, double b) throws CalculationException {
		return a * b;
	}

}
