package core;

import exceptions.CalculationException;

public class Power implements Operations{

	@Override
	public double makeOperation(double a, double b) throws CalculationException {
		
		return Math.pow(a,b);
	}
	

}
