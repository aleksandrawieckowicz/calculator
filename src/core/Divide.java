package core;

import exceptions.CalculationException;

public class Divide implements Operations {

	@Override
	public double makeOperation(double a, double b) throws CalculationException {
		if (b == 0) {
			throw new CalculationException("Nie dziel przez zero debilu");
		}
		return a / b;
	}

}
