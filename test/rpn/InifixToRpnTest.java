package rpn;

import java.util.ArrayList;
import java.util.List;

import org.assertj.core.api.Assertions;
import org.junit.Test;

import exceptions.RpnException;

public class InifixToRpnTest {

	private InifixToRpn inifixToRpn = new InifixToRpn();

	@Test
	public void testVerifyString1() {
		String s1 = "90";
		Assertions.assertThat(inifixToRpn.getNumberFromString(s1, 0)).isEqualTo("90");
	}

	@Test
	public void testVerifyString2() {
		String s2 = "9+9";
		Assertions.assertThat(inifixToRpn.getNumberFromString(s2, 0)).isEqualTo("9");
	}

	@Test
	public void testVerifyString3() {
		String s3 = "+";
		Assertions.assertThat(inifixToRpn.getNumberFromString(s3, 0)).isEqualTo("+");
	}

	@Test
	public void testVerifyString4() {
		String s4 = "2.2-9";
		Assertions.assertThat(inifixToRpn.getNumberFromString(s4, 0)).isEqualTo("2.2");
	}

	@Test
	public void testConvertToRpn1() throws RpnException {
		String s = "90";
		List<String> list = new ArrayList<String>();
		list.add("90");
		Assertions.assertThat(inifixToRpn.convertToRpn(s).containsAll(list));
	}

	@Test
	public void testConvertToRpn2() throws RpnException {
		String s = "9+9";
		List<String> list = new ArrayList<String>();
		list.add("9");
		list.add("9");
		list.add("+");
		Assertions.assertThat(inifixToRpn.convertToRpn(s).containsAll(list));
	}

	@Test
	public void testConvertToRpn3() throws RpnException {
		String s = "2.2-9";
		List<String> list = new ArrayList<String>();
		list.add("2.2");
		list.add("9");
		list.add("-");
		Assertions.assertThat(inifixToRpn.convertToRpn(s).containsAll(list));
	}

	@Test //TODO spr test
	public void testConvertToRpn4() throws RpnException {
		String s = "2+2";
		Assertions.assertThat(inifixToRpn.convertToRpn(s)).containsExactly("2", "2", "+");
	}
}
