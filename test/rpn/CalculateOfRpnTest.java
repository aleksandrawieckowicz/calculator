package rpn;

import java.util.ArrayList;
import java.util.List;

import org.assertj.core.api.Assertions;
import org.junit.Test;

import exceptions.CalculationException;

public class CalculateOfRpnTest {

	private CalculateOfRpn calculateOfRpn = new CalculateOfRpn();

	@Test
	public void testCalculateValueOfRpn() throws NumberFormatException, CalculationException {
		List<String> list = new ArrayList<>();
		list.add("2");
		list.add("2");
		list.add("+");
		Assertions.assertThat(Double.parseDouble(calculateOfRpn.calculateValueOfRpn(list))).isEqualByComparingTo(4.0);
	}
	
	@Test
	public void testCalculateValueOfRpn2() throws NumberFormatException, CalculationException {
		List<String> list = new ArrayList<>();
		list.add("12");
		list.add("2");
		list.add("3");
		list.add("4");
		list.add("*");
		list.add("10");
		list.add("5");
		list.add("/");
		list.add("+");
		list.add("*");
		list.add("+");
		Assertions.assertThat(Double.parseDouble(calculateOfRpn.calculateValueOfRpn(list))).isEqualByComparingTo(40.0);
	}

}
